import 'package:expenses/components/chart.dart';
import 'package:flutter/material.dart';

import 'dart:math';
import './components/transaction_form.dart';
import './components/transaction_list.dart';
import './models/transaction.dart';
main() => runApp(
      ExpensesApp(),
    );
final bgcolor = Colors.purple;

class ExpensesApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.purple,
        accentColor: Colors.amberAccent[200]
        ),
      home: MyHomePage(),
    );
  }
}

//Home Page
class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  
  final List<Transaction> _transactions = [
    Transaction(
      id: 't0',
      title: 'Conta antiga',
      value: 3140.76,
      date: DateTime.now().subtract(Duration(days: 33))
    ),
    Transaction(
      id: 't1',
      title: 'Novo Tenis de Corrida',
      value: 310.76,
      date: DateTime.now().subtract(Duration(days: 4))
    ),
    Transaction(
      id: 't2',
      title: 'Conta de Luz',
      value: 160.45,
      date: DateTime.now().subtract(Duration(days: 3))
    )
    
  ];

  List<Transaction> get _recentTreansactions{
    return _transactions.where((tr){
      return tr.date.isAfter(DateTime.now().subtract(
          Duration(days:7),
        ));
    }).toList();
  }
  _addTransaction( String title, double value){
    final newTransaction = Transaction(
      id: Random().nextDouble().toString(),
      title: title,
      value: value,
      date: DateTime.now()
    );
    setState(() {
      _transactions.add(newTransaction);
    });
    Navigator.of(context).pop();
  }
  _openTransactionFormModal(){
    showModalBottomSheet(
      context: context, 
      builder: (_){
        return TransactionForm(_addTransaction);
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        title: Text('Personal Expenses'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () => _openTransactionFormModal(),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Chart(_recentTreansactions),
              TransactionList(_transactions),
            ]),
      ),
      floatingActionButton: FloatingActionButton(
        child:Icon(Icons.add),
        onPressed: () => _openTransactionFormModal(),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
